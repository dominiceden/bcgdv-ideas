const apiNamespace = '/api/ideas';

export const getIdeas = () => {
  return $.ajax({
    url: apiNamespace,
    method: 'GET',
    dataType: 'json'
  });
}


export const createIdea = () => {
  return $.ajax({
    url: apiNamespace,
    method: 'POST',
    dataType: 'json'
  });
}

export const updateIdea = (id, ideaData) => {
  console.log(ideaData);
  return $.ajax({
    url: apiNamespace + `/${id.toString()}`,
    method: 'PATCH',
    dataType: 'json',
    data: {idea: ideaData}
  });
}

export const deleteIdea = (id) => {
  return $.ajax({
    url: apiNamespace + `/${id.toString()}`,
    method: 'DELETE',
    dataType: 'json'
  });
}
