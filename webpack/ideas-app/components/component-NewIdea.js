import React from 'react';

const NewIdea = (props) => {
  return (
    <div className="IdeaItem IdeaItem-new" onClick={() => {props.initNewIdea()}}>
      {props.newIdea.id ?
        <form>
          <div className="form-group">
            <input className="form-control" type="text" placeholder="Title" name="title"
                   onBlur={(event) => {props.ideaFieldBlurred(event, props.newIdea.id)}}
            />
          </div>
          <div className="form-group u-marginBottom0">
            <textarea className="form-control" placeholder="Body" name="body"
                      onBlur={(event) => {props.ideaFieldBlurred(event, props.newIdea.id)}}
            />
          </div>
          <i className="ion-ios-checkmark-outline center-block text-center u-marginTop10"
             onClick={() => {props.saveNewIdea(props.newIdea)}}>
          </i>
        </form>
        :
        <i className="ion-ios-plus-empty"></i>
      }
    </div>
  )
}

export default NewIdea;
