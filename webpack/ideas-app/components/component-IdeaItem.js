import React from 'react';
import { RIEInput, RIETextArea } from 'riek'

const IdeaItem = (props) => {
  return (
    <div className="IdeaItem">
      <i className="ion-ios-trash IdeaItem-delete" onClick={() => {props.deleteIdeaHandler(props.id)}}></i>
        <RIEInput
          value={props.title}
          change={props.updateIdeaHandler}
          propName='title'
          className="IdeaItem-title"
          validate={() => {return true}}
          beforeFinish={() => {props.setEditedIdea(props.id)}}
        />
      <RIETextArea
        value={props.body}
        change={props.updateIdeaHandler}
        propName='body'
        className="IdeaItem-body"
        validate={() => {return true}}
        beforeFinish={() => {props.setEditedIdea(props.id)}}
      />
    </div>
  )
}

export default IdeaItem;

// Set the ID of the currently editedIdea when clicked, use that to send update to API.
