import React from 'react';
import IdeaItem from './component-IdeaItem';

const IdeaList = (props) => {
  const ideaItems = props.ideas.map((idea) => {
    return (
      <IdeaItem
        key={idea.id}
        id={idea.id}
        title={idea.title}
        body={idea.body}
        setEditedIdea={props.setEditedIdea}
        updateIdeaHandler={props.updateIdeaHandler}
        deleteIdeaHandler={props.deleteIdeaHandler}
      />
    );
  });

  return (
    <div>
      {ideaItems}
    </div>
  )
}
export default IdeaList;
