import React from 'react'
import update from 'react-addons-update';

import LoadingSpinner from '../components/component-LoadingSpinner';
import IdeaList from '../components/component-IdeaList';
import NewIdea from '../components/component-NewIdea';
import { getIdeas, createIdea, updateIdea, deleteIdea } from '../api/ideas';

class AppContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ideas: [],
      newIdea: {},
      editedIdea: {}
    }
  }

  componentDidMount() {
    setTimeout(() => {
      getIdeas().then((data) => { // Get Ideas from API.
        let filteredData = data.filter((idea) => {return idea.title != null}); // Filter out ideas with no title.
        this.setState({ideas: filteredData});
      });
    }, 2000);
  }

  initNewIdea = (event) => {
    if (this.state.newIdea.id == null) {
      createIdea().then((data) => { // Get new Idea ID from API.
        var newState = update(this.state, {
          newIdea: {
            id: { $set: data.id }
          }
        });
        this.setState(newState);
      });
    }
  }

  ideaFieldBlurred = (event, Id) => {
    var newState = update(this.state, {
      newIdea: {
        [event.target.name]: { $set: event.target.value }
      }
    });
    this.setState(newState, () => {
      updateIdea(Id, this.state.newIdea).then((data) => { // Update new Idea ID via API.
        console.log(data);
      });
    });
  }

  saveNewIdea = (newIdea) => {
    var newState = update(this.state, {
      ideas: {$push: [newIdea]}, // Add the new Idea to state.
      newIdea: {$set: {}} // Reset new Idea form to empty.
    });
    this.setState(newState);
  }

  updateIdeaHandler = (obj) => {
    setTimeout(() => { // Add a delay to allow the ID of the idea being edited to be set in state.
      var key = Object.keys(obj)[0];

      var ideas = this.state.ideas;
      var ideaIndex = ideas.findIndex((idea) => { // Get the index of the idea being edited in the current ideas array.
        return idea.id == this.state.editedIdea.id;
      });

      var updatedIdea = update(ideas[ideaIndex], { // Update the idea with the changed data.
        [key]: { $set: obj[key] }
      });

      var newState = update(this.state, { // Splice the new idea into state, in its existing position.
        ideas: { $splice: [[ideaIndex, 1, updatedIdea]] }
      });

      this.setState(newState, () => {
        updateIdea(this.state.editedIdea.id, this.state.ideas[ideaIndex]); // Update idea via API.
      });
    }, 50);
  }

  setEditedIdea = (Id) => {
    console.log('call setEditedIdea');
    var newState = update(this.state, {
      editedIdea: {
        id: { $set: Id }
      }
    });
    this.setState(newState);
  }

  deleteIdeaHandler = (ideaId) => {
    deleteIdea(ideaId).then((data) => { // Get new Idea ID from API.
      var newState = update(this.state, {
        ideas: {$set: this.state.ideas.filter((idea) => { return idea.id != ideaId}) }
      });
      this.setState(newState);
    });
  }

  render() {
    if (this.state.ideas.length == 0) {
      return (
        <LoadingSpinner />
      )
    }
    else {
      return (
        <div>
          <IdeaList
            ideas={this.state.ideas}
            updateIdeaHandler={this.updateIdeaHandler}
            deleteIdeaHandler={this.deleteIdeaHandler}
            setEditedIdea={this.setEditedIdea}
           />
          <NewIdea
            initNewIdea={this.initNewIdea}
            newIdea={this.state.newIdea}
            ideaFieldBlurred={this.ideaFieldBlurred}
            saveNewIdea={this.saveNewIdea}
          />
        </div>
      )
    }
  }
}


export default AppContainer;
