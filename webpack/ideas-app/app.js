import React from 'react';
import ReactDOM from 'react-dom';

import AppContainer from './containers/container_app';

$(document).ready(() => {
  ReactDOM.render(
    <AppContainer />,
    document.getElementById('IdeasAppContainer')
  );
});
