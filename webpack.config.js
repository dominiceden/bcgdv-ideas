module.exports = {
  context: __dirname,
  entry: {
    bcgdv_ideas: './webpack/ideas-app/app.js'
  },
  output: {
    publicPath: '/',
    filename: '[name]-bundle.js',
    path: __dirname + '/app/assets/javascripts'
  },
  watch: true,
  module: {
    loaders: [{
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets:['es2015', 'react'],
        plugins: ["transform-class-properties"]
      }
    }]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  }
};
