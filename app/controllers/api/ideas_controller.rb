class Api::IdeasController < ApplicationController
  protect_from_forgery with: :null_session

  def index
    ideas = Idea.all.order(:created_at)
    render :json => ideas.as_json, status: 200
  end

  def create # Create an Idea without any params - we only need the ID of the created record.
    idea = Idea.create
    render :json => idea.as_json, status: 201
  end

  def update
    idea = Idea.find(params[:id])
    if idea.update(idea_params)
      render :json => idea.as_json, status: 200
    else
      render :json => { error: idea.errors.full_messages.join(', ') }, status: 400
    end
  end

  def destroy
    idea = Idea.find(params[:id])
    if idea.destroy
      render :json => { success: "Idea was successfully deleted." }, status: 200
    else
      render :json => { error: idea.errors.full_messages.join(', ') }, status: 400
    end
  end

  private

  def idea_params
    params.require(:idea).permit(:title, :body)
  end

end
