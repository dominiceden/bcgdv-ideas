# README

## Config

Run `rails db:seed` to populate the database with 15 ideas.

## Starting the application

1. Start Rails server with `rails s` command.
2. Run `webpack` to watch the files and recompile on change.
3. Navigate to localhost:3000 to view the app.
